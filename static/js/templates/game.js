var commandHistory = [];
var historyIterator = 0;
var waitingForCommandExecution = false;

var start;
var end;


function commandExe() {
    historyIterator = commandHistory.length;

    var value = $('#consoleInput').val();
    if (value.trim() != "") {
        executeCommand(value);
    }
}


function executeCommand(command) {

    if (waitingForCommandExecution) {
        return false
    }

    start = new Date().getTime();

    waitingForCommandExecution = true;
    var data = {command: command};

    $.ajax({
        dataType: "json",
        url: "/commandExe/",
        timeout: my_timeout,
        data: data,
        success: function (data) {

            changeView(command, data);

            // in the battle
            // we gets response from console
            // with some delay

            $('#consoleInput').val("");

            afterCommand(data)

        },
        error: errorInAjaxResponse
    });
}

function afterCommand(data) {

    appendResponseMessage(data.message);

    scrollInput();

    waitingForCommandExecution = false;
}

function sendMessage() {

    $.ajax({
        dataType: "json",
        url: "/reports/response/",
        data: {response: $("#reportResponse").val()},
        timeout: my_timeout,
        success: function (data) {
            $('.close-reveal-modal').trigger('click');
        },
        error: errorInAjaxResponse
    });

}


function errorInAjaxResponse(jqXHR, exception) {

    if (jqXHR.status === 0) {
        alert('Stracono połączenie z internetem');
    } else if (jqXHR.status == 404) {
        alert('Requested page not found. [404]');
    } else if (jqXHR.status == 500) {
        alert('Internal Server Error [500].');
    } else if (exception === 'parsererror') {
        alert('Requested JSON parse failed.');
    } else if (exception === 'timeout') {
        alert('Time out error.');
    } else if (exception === 'abort') {
        alert('Ajax request aborted.');
    } else {
        alert('Uncaught Error.\n' + jqXHR.responseText);
    }

    waitingForCommandExecution = false;

}
