my_timeout = 1000;

function LogInFunction() {
    var data = {username: $('#username').val(), password: $('#password').val()};

    $.ajax({
        dataType: "json",
        url: "/login/",
        timeout:my_timeout,
        data: data,
        success: function (data) {
            if (data.status === 'success') {

                window.location.href = "/game/";

            } else if (data.status === 'pass_err') {
                alert('Błąd logowania');
                console.log(data);
            } else {
                alert('Błąd logowania');
                console.log(data);
            }
        },
        error: function (jqXHR, exception) {
            if (jqXHR.status === 0) {
                alert('Stracono połączenie z internetem');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
}
