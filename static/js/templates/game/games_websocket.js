function initialiseWebSocket() {

    var player_nick = $('#playerNick').html();
    var ws = new WebSocket('ws://' + window.location.host
        + '/ws?username=' + player_nick);

    ws.onmessage = function (message) {

        chat_message = message.data;

        if (isSpecialMessage(chat_message)) {
            executeSpecialMessages(chat_message);
        } else {
            appendChatMessage(chat_message);
            scrollInput();
        }

    };
}




