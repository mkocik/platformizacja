$(document).ready(function () {

    $("#createWorld").on("click", function (e) {

        $("#worldWasCreatedInfo").hide();

        var link = this;

        e.preventDefault();

        $("#worldIsCreating").show();
        window.location = link.href;
    });

    $("#createMap").on("click", function (e) {

        $("#worldWasCreatedInfo").hide();

        var link = this;

        e.preventDefault();

        $("#mapIsCreating").show();
        window.location = link.href;
    });

});
