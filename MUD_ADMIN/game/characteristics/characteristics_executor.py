# -*- coding: utf-8 -*-
from __future__ import unicode_literals

class Characteristics(object):
    def __init__(self, name, f):
        self.name = name
        self.f = f

    def __call__(self, player):
        return self.f(player)
