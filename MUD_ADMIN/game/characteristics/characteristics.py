# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from language.PL import *
from MUD_ADMIN.game.characteristics.characteristics_executor import Characteristics


all_characteristics = [
    Characteristics(CHARISMA , lambda player: int(round(player.get_attribute_value("Życie") + + player.get_parameter("Siła") +
                                                player.get_skill_value("Złodziejskie palce")))),

    #Characteristics(ATTACK, lambda player: player.get_parameter("Zręczność")),

    #Characteristics(DEFENCE, lambda player: player.get_parameter("Zwinność")),

    #Characteristics(MIN_DMG, lambda player: int(floor(player.get_parameter("Siła") * 0.2) + ceil(player.get_parameter("Siła") * 0.3 * 0))),

    #Characteristics(MAX_DMG, lambda player: int(floor(player.get_parameter("Siła") * 0.2) + ceil(player.get_parameter("Siła") * 0.3 * 1))),

    Characteristics(ARMOR, lambda player: player.natural_armor()),

    #Characteristics(ATTACK_SPEED, lambda player: properties.DEFAULT_ATTACK_TIME)

    ]



