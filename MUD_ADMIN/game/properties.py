# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division  #forces float division

COMMAND_TIMEOUT = 8000  #in miliseconds

STARTING_HP = 100
STARTING_GOLD_VALUE = 10
STARTING_STAMINA = 100

# changed in usr/local/lib/python2.7/dist-packages/django/core/management/commands/runserver.py
# during server configuration set DEFAULT_PORT from properties
# do not forget to set self.addr from 127.0.0.1 to 0
DEFAULT_PORT = 8001


user_colors = [
    "gray", "black", "red", "maroon",
    "olive", "lime", "green", "brown",
    "aqua", "teal", "blue", "navy",
    "fuchsia", "purple", "pink", "orange"
]


def armor_durability(player):
    return player.get_parameter("Wytrzymałość") / 10

def attribute_points_for_lvl(player):
    val = player.get_parameter("Inteligencja")/10
    if val > 1:
        return val
    return 1

def skills_points_for_lvl(player):
    val = player.get_parameter("Inteligencja")/20
    if val > 1:
        return val
    return 1

def experience_function(player_experience):
    exp_to_lvl = 10
    lvl = 1

    while player_experience >= exp_to_lvl:
        player_experience = player_experience - exp_to_lvl
        exp_to_lvl *= 10
        lvl += 1
    return lvl





