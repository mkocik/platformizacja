from MUD_ADMIN.game.characteristics.characteristics import all_characteristics


class LivingCreature(object):
    def __init__(self, entity):
        self.entity = entity

    def get_agility(self):
        pass

    def get_parameter(self, name):
        return self.entity.get_parameter(name)

    def get_attribute(self,name):
        return self.entity.get_attribute(name)

    def get_characteristics(self, name):
        for char in all_characteristics:
            if char.name == name:
                return char(self.entity)
        return None

    def get_defence(self):
        from game_app.models.PlayerModel import Player

        if type(self.entity) is Player:
            return self.entity.get_parameter("Pancerz")
        else:
            return 0
