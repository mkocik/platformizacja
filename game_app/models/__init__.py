
from game_app.models.PlayerModel import *

from game_app.utils.ModuleManager import module_manager
import importlib.machinery

#from modules.MapModule.models.MapModel import *  #Map is always included!!

for key, value in module_manager.get_all_modules().items():
    try:
        path = value.return_path_to_model()
        try:
            loader = importlib.machinery.SourceFileLoader(key+'_model', path)
            loader.load_module() #loads models
        except:
            raise Exception("Not found file in: " + value)
    except:
        raise Exception("Module " + key + " has not return_path_to_model() method implemented!")