# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from random import randint

from django.db import models
from django.contrib.auth.models import User

from MUD_ADMIN.game import properties
from language.PL import *
from interfaces.living_creature import LivingCreature
from game_app.utils.discover.discover_attributes_functions import attributes_functions_dictionary


class AllParameter(models.Model):
    class Meta:
        app_label = "game_app"

    name = models.CharField(max_length=45, null=False)
    default_val = models.IntegerField(default=0)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class PlayableParameter(AllParameter):
    class Meta:
        app_label = "game_app"

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class StaticParameter(AllParameter):
    class Meta: app_label = "game_app"

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.__str__()


class AllAttribute(models.Model):
    class Meta: app_label = "game_app"

    name = models.CharField(max_length=45, null=False)
    default_val = models.IntegerField(default=0)
    function_name = models.CharField(max_length=45, null=False)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.__str__()


class PlayableAttribute(AllAttribute):
    class Meta: app_label = "game_app"

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class StaticAttribute(AllAttribute):
    class Meta: app_label = "game_app"

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.__str__()

class ReportTime(models.Model):
    class Meta: app_label = "game_app"

    player = models.ForeignKey('Player')
    start_date = models.DateTimeField(null=False)
    end_date = models.DateTimeField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.player.user.username + " " + str(self.start_date) + " " + str(self.end_date)


class ReportResponse(models.Model):
    class Meta: app_label = "game_app"

    player = models.ForeignKey('Player')
    response = models.TextField(null=False)
    date = models.DateTimeField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.player.user.username + " " + str(self.date)


class Player(models.Model):
    class Meta:
        app_label = "game_app"

    from MUD_ADMIN.game.properties import user_colors

    user = models.OneToOneField(User)
    location = models.ForeignKey('Map', null=False, default=0)
    gold = models.IntegerField(default=0)
    experience = models.IntegerField(default=0)
    distribution_points = models.IntegerField(default=0)
    skills_points = models.IntegerField(default=0)
    color = models.IntegerField(default=randint(0, len(user_colors) - 1))
    #creature = models.IntegerField(default=-1)
    # creature == -1 - not fighting
    # creature == 0 - for monsters,
    # creature  > 0 - for other user, as his primary key
    #asset_in_map = models.ForeignKey('AssetInMap', null=True, blank=True, default=None, related_name="asset_in_map")
    #in_trade = models.BooleanField(default=False, null=False)
    #stealing_from = models.ForeignKey('AssetInMap', null=True, blank=True, default=None, related_name="stealing_from")

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.user.username

    def get_attribute_value(self, name):
        attr = self.get_attribute(name)
        if attr is None:
            return 0
        else:
            return attr.current_state

    def get_skill_value(self, name):
        skill = self.get_skill(name)
        if skill is None:
            return 0
        else:
            return skill.value

    def increase_lvl(self):
        self.add_distribution_points(properties.attribute_points_for_lvl(LivingCreature(self)))
        self.add_skill_points(properties.skills_points_for_lvl(LivingCreature(self)))
        self.regain_all_attributes()


    def get_attribute_mods(self, function_name): #from function in properties
        creature = LivingCreature(entity=self)
        return attributes_functions_dictionary[function_name](creature)

    def get_all_attributes(self):
        return PlayerAttribute.objects.filter(player=self)


    def natural_armor(self):
        try:
            natural_armor = properties.armor_durability(LivingCreature(self))
            return natural_armor
        except:
            return 0

    def get_all_attributes_with_mods(self):
        attributes = PlayerAttribute.objects.filter(player=self)
        for attr in attributes:
            static_attr = StaticAttribute.objects.get(name=attr.attribute.name)
            attr.max_state = static_attr.default_val + self.get_attribute_mods(static_attr.function_name)
        return attributes

    def regain_all_attributes(self):
        try:
            attrs = PlayerAttribute.objects.filter(player=self)
            for attr in attrs:
                static_attr = StaticAttribute.objects.get(name=attr.attribute.name)
                attr.current_state = static_attr.default_val + self.get_attribute_mods(static_attr.function_name)
                attr.save()
        except:
            return None


    def increase_gold(self, amount):
        self.gold = self.gold + amount
        self.save()

    def add_distribution_points(self, amount):
        self.distribution_points = self.distribution_points + amount
        self.save()

    def add_skill_points(self, amount):
        self.skills_points = self.skills_points + amount
        self.save()

    def remove_distribution_points(self, amount):
        self.distribution_points = self.distribution_points - amount
        self.save()

    def get_parameter_object(self, name):
        try:
            parameters = PlayerParameter.objects.filter(player=self)

            for param in parameters:
                if param.get_name().lower() == name.lower():
                    return param

            return None
        except PlayableParameter.DoesNotExist:
            return None


    def get_attribute(self, name):
        try:
            attributes = PlayerAttribute.objects.filter(player=self)
            for attr in attributes:
                if attr.attribute.name.lower() == name.lower():
                    return attr
        except:
            return None
        return None

    def get_all_skills(self):
        return PlayerSkill.objects.filter(player=self)

    def get_skill(self, id):
        try:
            skills = Skill.objects.all()
            counter = 0
            for skill in skills:
                if(id-1==counter):
                    return skill
                counter+=1
        except:
            return None
        return None

    def get_skill_by_name(self,name):
       try:
           skill =  Skill.objects.get(name=name)
           player_skill = PlayerSkill.objects.get(player=self,skill=skill)
           return player_skill
       except:
            return None

    def add_skill(self,skill,value):
        try:
            player_skill = PlayerSkill.objects.get(skill=skill,player=self)
            player_skill.value = player_skill.value + value
            player_skill.save()
            self.skills_points = self.skills_points - value
            self.save()
        except PlayerSkill.DoesNotExist:
            player_skill = PlayerSkill(player=self,skill=skill,value=value)
            player_skill.save()
            self.skills_points = self.skills_points - value
            self.save()

    def add_to_param(self, param, amount):
        attributes_dict_before = {}
        for attr in PlayerAttribute.objects.filter(player=self):
            static_attr = StaticAttribute.objects.get(name=attr.attribute.name)
            attributes_dict_before[attr.attribute.name] = self.get_attribute_mods(static_attr.function_name)

        param.increase_param(amount)

        attributes_dict_after = {}
        for attr in PlayerAttribute.objects.filter(player=self):
            static_attr = StaticAttribute.objects.get(name=attr.attribute.name)
            attributes_dict_after[attr.attribute.name] = self.get_attribute_mods(static_attr.function_name)

        for attr_name,value in attributes_dict_before.items():
            difference = attributes_dict_after[attr_name] - attributes_dict_before[attr_name]
            if difference != 0:
                try:
                    attr_to_change = AllAttribute.objects.get(name=attr_name)
                    player_attr = PlayerAttribute.objects.get(player=self,attribute=attr_to_change)
                    player_attr.current_state+=difference
                    player_attr.save()
                except:
                    return None


    def get_level(self):
        return properties.experience_function(self.experience)

    def increase_experience(self, xp):
        self.experience = self.experience + xp
        self.save()


    def get_parameter(self, name):
        from MUD_ADMIN.game.characteristics.characteristics import all_characteristics
        if name == 'Pancerz':
            return ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(self)
        else:
            try:
                parameter = PlayableParameter.objects.get(name=name)
                parameter_object = PlayerParameter.objects.get(player=self, parameter=parameter)
                parameter_value = parameter_object.state
                return parameter_value
            except:
                return

    def get_all_parameters(self):
        return PlayerParameter.objects.filter(player=self)

    def get_all_parameters_with_mods(self):
        params = PlayerParameter.objects.filter(player=self)
        for param in params:
            param.state = self.get_parameter(param.parameter.name)
        return params


    #function for tests
    def set_location(self, location):
        self.location = location
        self.save()

    def set_parameter(self, parameter_name, state):
        try:
            if state is not None:
                parameter = PlayableParameter.objects.get(name=parameter_name)
                player_parameter = PlayerParameter.objects.get(player=self, parameter=parameter)
                player_parameter.state = state
                player_parameter.save()
        except:
            return


class PlayerParameter(models.Model):
    class Meta: app_label = "game_app"

    player = models.ForeignKey('Player', null=False)
    parameter = models.ForeignKey('PlayableParameter', null=False)
    state = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.player.__str__() + ' - ' + self.parameter.__str__()

    def get_name(self):
        return self.parameter.name

    def get_param_id(self):
        return self.pk

    def increase_param(self, amount):
        self.state = self.state + amount
        self.save()



class PlayerAttribute(models.Model):
    class Meta: app_label = "game_app"

    player = models.ForeignKey('Player', null=False)
    attribute = models.ForeignKey('AllAttribute', null=False)
    current_state = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.attribute.__str__()

    def get_name(self):
        return self.attribute.name

    def get_attr_id(self):
        return self.id


class Skill(models.Model):
    class Meta: app_label = "game_app"
    ordering = ('name',)

    name = models.CharField(null=False,max_length=255)
    description = models.TextField()

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name.__str__()


class PlayerSkill(models.Model):
    class Meta:
        app_label = "game_app"

    skill = models.ForeignKey('Skill',null=False)
    player = models.ForeignKey('Player',null=False)
    value = models.IntegerField(default=0)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.skill.__str__() + " " + self.player.user.__str__()


class Characteristic(models.Model):
    class Meta: app_label = "game_app"

    name = models.CharField(max_length=255, null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name