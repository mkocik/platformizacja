# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from game_app.utils.ModuleManager import module_manager

class CommandValidation(object):
    def is_command_valid(self, command_name):
        is_normal_command = module_manager.get_command_by_name(command_name)
        is_numeric_command = self.is_numeric_command(command_name)

        return is_normal_command or is_numeric_command


    def is_numeric_command(self, command_name):
        return command_name.isdigit()

    def is_authenticated(self, user):
        return user.is_authenticated()
