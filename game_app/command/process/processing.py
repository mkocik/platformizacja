# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.command.process.response import ResponseObject, Request
from game_app.command.process.validation import CommandValidation
from game_app.controllers.basic_controllers import UserController
from game_app.utils.ModuleManager import module_manager

class ProcessCommand(object):
    def __init__(self, request):
        self.original_request = request
        self.request = Request(request)
        self.message_array = self.request.message_array
        self.name = self.request.command_name
        self.validation = CommandValidation()

        self.player = UserController.get_player_from_user(self.request.user)
        self.response = ResponseObject(self.player)


    def execute(self):

        if self.validation.is_command_valid(self.name) is False:
            return self.response.invalid_command()

        if self.validation.is_authenticated(self.request.user) is False:
            return self.response.not_authenticated()

        #if self.name in manual_commands:
            #return ManualCommand(self.request).execute()


        command_properties = module_manager.get_command_by_name(self.name) #get_command_properties[self.name]

        if command_properties.is_available(self.player) is False:
            return self.response.no_effect('To polecenie nie jest w tej chwili dla ciebie dostępne.')

        return self.do_command()

    def do_command(self):
        command = CommandFactory.get_command_object(self.original_request)
        if command:
            return command.execute()
        else:
            return ResponseObject().incorrect_syntax()

class CommandFactory:
    @classmethod
    def get_command_object(cls, request):
        request = Request(request)
        command_name = request.command_name
        arguments = len(request.message_array)

        if command_name in [command.name for command in module_manager.get_all_commands()]:
            command_properties = module_manager.get_command_by_name(command_name)
            CommandClass = command_properties.command_class

            #tutaj interfejs ma zwrocic te rzeczy
            if command_name in module_manager.get_module("MapModule").get_directions_names():
                command_object = module_manager.get_module("MapModule").move_command(request, command_name)
                print(command_object)

                if arguments == 1:
                    return command_object
                else:
                    return None

            else:
                command_object = CommandClass(request)
                only_one_argument = command_properties.only_one_argument

                if only_one_argument and arguments != 1:
                    return None
                else:
                    return command_object

        else:
            return None