# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import abc

from game_app.command.highlighter import Highlighter
from game_app.controllers.basic_controllers import UserController


class Command():
    def __init__(self, request):
        from game_app.command.process.response import ResponseObject

        self.request = request
        self.user = self.request.user

        self.response = ResponseObject(UserController.get_player_from_user(self.request.user))
        self.message_array = self.request.message_array
        self.name = self.request.command_name

    @abc.abstractmethod
    def execute(self):
        return

    def get_player_from_request(self):
        return UserController.get_player_from_user(self.user)


class HelpCommand(Command):
    def execute(self):
        from game_app.utils.ModuleManager import module_manager
        from game_app.command.settings.command_types import CheatingTypeCommand

        all_commands = module_manager.get_all_commands()

        command_types = []

        for command in all_commands:
            if command.type not in command_types and command.type != CheatingTypeCommand:
                command_types.append(command.type)

        results_commands = []
        #print(command_types)
        for type in command_types:
            tmp = []
            for command in all_commands:
                if command.type == type:
                    tmp.append(command)
            results_commands.append(tmp)


        manual_message = ''

        for commands in results_commands:
            manual_message += self.list_one_command_category(commands)

        manual_message = Highlighter.manual(manual_message)
        return self.response.success(manual_message)

    def list_one_command_category(self, commands):
        if len(commands)>0:
            manual_message = Highlighter.bold(commands[0].type.title) + '<br>'
            commands = sorted(commands, key=lambda x: x.order)

            for command in commands:
                command_name = command.name
                shortcut = command.shortcut
                description = command.get_description()

                command_use = Highlighter.underline(command_name)

                if shortcut is not None:
                    command_use = command_use + ", " + shortcut
                    command_use = Highlighter.underline(command_use)

                message = command_use + " - " + description
                message += "<br>"
                manual_message += message

            manual_message += "<br>"

            return manual_message
        return ""


