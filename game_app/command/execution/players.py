# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.command.highlighter import Highlighter
from game_app.command.execution.others import Command
from MUD_ADMIN.game.properties import *
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics


class GetSkills(Command):
    def execute(self):
        from game_app.models.PlayerModel import Skill,PlayerSkill
        player = self.get_player_from_request()
        message_array = self.message_array

        if len(message_array) == 2:

            if message_array[1] == "wszystkie" or message_array[1] == "all":
                message = ""
                skills = Skill.objects.all()
                counter = 1

                for skill in skills:
                    message+= Highlighter.bold(str(counter)) + ". " + Highlighter.bold(skill.name) +  ": " + skill.description  + "<br>"
                    counter+=1

                if message=="":
                    message = "Brak dostępnych umiejętności"

                return self.response.success(message)

            elif message_array[1] == "moje" or message_array[1] == "my":
                message = ""
                skills = PlayerSkill.objects.filter(player=player)

                for skill in skills:
                    message+= skill.skill.name +  ": " + Highlighter.bold(str(skill.value)) + "<br>"

                if message=="":
                    message = "Brak przydzielonych punktów umiejętności"

                return self.response.success(message)

            else:
                message = "Składnia: %s wszystkie(all)/moje(my)" % self.name
                return self.response.bad_syntax(message)

        else:
            message = "Składnia: %s wszystkie(all)/moje(my)" % self.name
            return self.response.bad_syntax(message)


class GetCharacterCard(Command):
    def execute(self):
        player = self.get_player_from_request()
        message_array = self.message_array
        if len(message_array) == 1:
            message=Highlighter.color("Atrybuty postaci: <br>", "blue")
            attributes = player.get_all_attributes_with_mods()
            for attr in attributes:
                message+=attr.attribute.name + " " + Highlighter.bold(str(attr.current_state)) + "/" + Highlighter.bold(str(attr.max_state)) + "<br/>"

            message+="<br>"

            message += Highlighter.color("Parametry postaci: <br>", "blue")
            parameters = player.get_all_parameters_with_mods()
            for param in parameters:
                message+=param.parameter.name + ": " + Highlighter.bold(str(param.state)) + "<br/>"

            message+="<br>"

            message += Highlighter.color("Cechy postaci: <br>", "blue")
            for char in all_characteristics:
                message += char.name + ": " + Highlighter.bold(str(char(player))) + "<br/>"

            message+="<br>"

            message += Highlighter.color("Doświadczenie:", "blue") + str(player.experience) + "<br>"
            message += Highlighter.color("Poziom:", "blue") + str(experience_function(player.experience)) + "<br>"

            message+="<br>"

            skills = player.get_all_skills()

            if(len(skills) > 0):
                message += Highlighter.color("Umiejętności: <br>", "blue")
                for skill in skills:
                    message+=skill.skill.name + ": " + Highlighter.bold(str(skill.value)) + "<br/>"

                message+="<br>"


            return self.response.success(message)
        else:
            message = "Składnia: %s" % self.name
            return self.response.bad_syntax(message)


class SetCommand(Command):
    def execute(self):

        message_array = self.message_array
        player = self.get_player_from_request()

        if len(message_array) == 1:

            amount = str(player.distribution_points) + " pkt"
            amount = Highlighter.bold(amount)
            skill_amount = str(player.skills_points) + " pkt"
            skill_amount = Highlighter.bold(skill_amount)
            command_help = Highlighter.bold("przydziel nazwa_atrybutu/numer_umiejetnosci liczba_punktów")

            message = "W tej chwili posiadasz: " + str(
                amount) + " atrybutów oraz "+str(skill_amount)+" umiejętności do podziału. <br> Zrobisz to poleceniem: " + command_help

            return self.response.success(message)

        elif len(message_array) == 3:

            del message_array[0]

            try:

                number = int(message_array[0])
                skill = player.get_skill(number)

                try:
                    amount = int(message_array[1])

                except ValueError:
                    message = "Ilość punktów musi być liczbą naturalną."
                    return self.response.bad_syntax(message)

                if amount <= 0:
                    message = "Ilość punktów musi być liczbą dodatnią."
                    return self.response.bad_syntax(message)

                if amount > player.skills_points:
                    message = "Posiadasz  "+str(player.skills_points)+" pkt umiejętności do podziału."
                    return self.response.bad_syntax(message)

                player.add_skill(skill,amount)

                message = "Prawidłowo rozdzielono punkty umiejętności."
                return self.response.success(message)
            except ValueError:

                param = player.get_parameter_object(message_array[0])

                if param is not None:

                    try:
                        amount = int(message_array[1])

                    except ValueError:
                        message = "Ilość punktów musi być liczbą naturalną."
                        return self.response.bad_syntax(message)

                    if amount <= 0:
                        message = "Ilość punktów musi być liczbą dodatnią."
                        return self.response.bad_syntax(message)

                    if amount <= player.distribution_points:
                        player.add_to_param(param, amount)
                        player.remove_distribution_points(amount)
                        amount = Highlighter.bold(str(amount))

                        message = "Prawidłowo rozdzielono %s pkt atrybutów." % amount
                        message = Highlighter.information(message)
                        return self.response.success(message)

                    else:
                        message = "Nie masz wystarczającej liczby punktów atrybutów."
                        return self.response.bad_syntax(message)

                else:
                    message = "Nie znaleziono atrybutu o podanej nazwie."
                    return self.response.bad_syntax(message)
            except:
                message = "Błędny indeks umiejętności. Sprawdź indeksy poleceniem wyświetlającym wszystkie umiejętności."
                return self.response.bad_syntax(message)

        else:
            message = "Składnia: %s nazwa_atrybutu/indeks_umiejetnosci liczba_punktów" % self.name
            return self.response.bad_syntax(message)

