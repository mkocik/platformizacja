# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class CommandType(object):
    pass

class MultiplayerTypeCommand(CommandType):
    title = 'Gra Multiplayer'

class OtherTypeCommand(CommandType):
    title = 'Inne'

class PlayerTypeCOmmand(CommandType):
    title = 'Gracz'

class CheatingTypeCommand(CommandType):
    title = 'Kody'
