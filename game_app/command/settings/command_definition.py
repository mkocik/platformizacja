# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from game_app.command.settings.command_types import *
from game_app.command.execution.multiplayer import WhisperCommand,SayCommand,PlayersCommand
from game_app.command.execution.others import HelpCommand
from game_app.command.execution.players import GetSkills,SetCommand,GetCharacterCard

class CommandProperties:
    def __init__(self, name, description, command_class=None, type=None, shortcut=None, only_one_argument=False,
                 order=99, availability_function = None):
        self.name = name
        self.description = description
        self.shortcut = shortcut
        self.command_class = command_class
        self.type = type
        self.only_one_argument = only_one_argument
        self.order = order
        self.availability_function = availability_function

    def is_available(self, player):
        if self.availability_function is None:
            return True
        else:
            return self.availability_function.function(player)

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description

    def get_command_class(self):
        return self.command_class

    def get_type(self):
        return self.type

    def get_shortcut(self):
        return self.shortcut

    def has_only_one_argument(self):
        return self.only_one_argument

    def get_order(self):
        return self.order


#############################
# Multiplayer game
#
player = CommandProperties("gracze", "zwraca graczy na bieżącym polu", PlayersCommand, MultiplayerTypeCommand, "g",
                           only_one_argument=True, order=1)
say = CommandProperties("powiedz", "mówi do graczy na bieżącym polu (powiedz wiadomość)", SayCommand,
                        MultiplayerTypeCommand, "p")
whisper = CommandProperties("szepnij", "pozwala szepnąc do gracza (szepnij nick_gracza wiadomość)", WhisperCommand,
                            MultiplayerTypeCommand, "sz")




#############################
# Others
#

skills = CommandProperties("umiejętności",
                           "umiejętności wszystkie(all)/moje(my)  wyświetla listę dostępnych umiejętności)", GetSkills,
                           PlayerTypeCOmmand, "skills", order=3)
sett = CommandProperties("przydziel",
                         "pozwala przydzielić punkty atrybutów/umiejętności (przydziel nazwa_atrybutu/numer_umiejętności ilość)",
                         SetCommand, PlayerTypeCOmmand, "add", order=4)
character_card = CommandProperties("karta", "karta - wyświetla kartę postaci gracza", GetCharacterCard,
                                   PlayerTypeCOmmand, "card", order=5)

helpp = CommandProperties("pomoc", "zwraca listę poleceń z opisem", HelpCommand, OtherTypeCommand, "?",
                          only_one_argument=True, order=1)



