# -*- coding: utf-8 -*-
'''from __future__ import unicode_literals

from game_app.command.settings.command_definition import *
from game_app.utils.discover.discover_command import command_properties_array

##################################################
# file contains all command settings required to processing user command
#

command_array = list(map(lambda x: x.name, command_properties_array))
command_description = list(map(lambda x: x.description, command_properties_array))
command_shortcut = list(map(lambda x: x.shortcut, command_properties_array))
get_command_from_shortcut = dict(zip(command_shortcut, command_array))

commands_and_shortcuts = command_array + command_shortcut
commands_and_shortcuts = list(filter(lambda x: x is not None, commands_and_shortcuts))
print("========")
print(commands_and_shortcuts)
##################################################
# commands divided to specific categories for manual listing
#

multiplayer_commands = list(filter(lambda x: x.type is MultiplayerTypeCommand, command_properties_array))
other_commands = list(filter(lambda x: x.type is OtherTypeCommand, command_properties_array))

manual_commands = [helpp.name, helpp.shortcut]'''






