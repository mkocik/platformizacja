#CommandHighlighter add CSS classes to all kind of messages
#to add styles to them
#related CSS file: /static/css/commands.css


class Highlighter:
    @staticmethod
    def frame(message):
        return "<div class='messageFrame'>" + message + "</div>"

    @staticmethod
    def indent(message):
        opening_tag = "<span class='indentedMessage'>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def underline(message):
        opening_tag = "<u>"
        closing_tag = "</u>"
        return opening_tag + message + closing_tag

    @staticmethod
    def color(message, color):
        opening_tag = "<span style='color:%s'>" % color
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def bold(message):
        opening_tag = "<b>"
        closing_tag = "</b>"
        return opening_tag + message + closing_tag

    @staticmethod
    def italic(message):
        opening_tag = "<i>"
        closing_tag = "</i>"
        return opening_tag + message + closing_tag

    @staticmethod
    def information(message):
        opening_tag = "<span class='informationMessage'>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def attention(message):
        opening_tag = "<span class='attentionMessage'>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def location(message):
        return Highlighter.information(message)

    @staticmethod
    def description(message):
        opening_tag = "<span class='descriptionMessage'>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def surroundings(message):
        opening_tag = "<span class='encouragingMessage'>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def wrong_command(message):
        return Highlighter.attention(message)

    @staticmethod
    def wrong_direction(message):
        opening_tag = "<span class='wrongDirectionMessage'>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def manual(message):
        opening_tag = "<span class=''>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def monster_name(message):
        opening_tag = "<b class='monsterNameMessage'>"
        closing_tag = "</b>"
        return opening_tag + message + closing_tag

    @staticmethod
    def monster_description(message):
        opening_tag = "<span class='monsterDescriptionMessage'>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def attack(message):
        return Highlighter.attention(message)

    @staticmethod
    def players(message):
        return Highlighter.information(message)

    @staticmethod
    def speaker_name(message):
        return Highlighter.bold(message)

    @staticmethod
    def talk(message):
        return Highlighter.information(message)

    @staticmethod
    def bad_syntax(message):
        return Highlighter.attention(message)

    @staticmethod
    def whisper(message):
        opening_tag = "<span class='secretMessage'>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag

    @staticmethod
    def battle(message):
        opening_tag = "<span class='battleMessage'>"
        closing_tag = "</span>"
        return opening_tag + message + closing_tag