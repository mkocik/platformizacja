# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime

from django.utils import html
from django.contrib.auth.decorators import user_passes_test
from django.contrib import auth
from django.http import HttpResponseRedirect
from django.utils.timezone import utc
from Platformizacja.settings import logger

from game_app.models.PlayerModel import *
from game_app.command.highlighter import Highlighter
from language.PL import *
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics
from game_app.utils.wrappers import *
from game_app.controllers.basic_controllers import *
from game_app.utils.ModuleManager import module_manager

@is_user_not_authenticated
def index(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('index.html', c)


def login(request):
    auth.logout(request)

    if request.method != "POST":
        c = {}
        c.update(csrf(request))
        return render_to_response('index.html', c)

    username = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(username=username, password=password)

    if user and user.is_active:

        auth.login(request, user)
        player = UserController.get_player_from_user(request.user)
        if player or request.user.is_superuser:
            return HttpResponseRedirect("/game/")

        c = {"message": "Brak gracza dla wybranego użytkownika. Skontaktuj się z administratorem"}
        c.update(csrf(request))
        auth.logout(request)

        return render_to_response('index.html', c)

    c = {"message": "Wprowadzono niepoprawne dane. Błędny login lub hasło. Spróbuj ponownie."}
    c.update(csrf(request))
    return render_to_response('index.html', c)


def logout(request):
    auth.logout(request)
    c = {}
    c.update(csrf(request))
    return render_to_response('index.html', c)


@is_request_method_post
def register(request):
    username = request.POST['username']
    password = request.POST['password']

    if_exists = User.objects.filter(username=username)
    if if_exists:
        c = {"message": "Użytkownik o takim loginie już istnieje."}
        c.update(csrf(request))
        return render_to_response('register.html', c)

    try:
        mapModuleObject = module_manager.get_module("MapModule")
        start_location = mapModuleObject.get_starting_location()

    except:
        c = {"message": "Pole startowe wybrane w pliku konfiguracyjnym nie istnieje."}
        c.update(csrf(request))
        return render_to_response('register.html', c)

    user = User.objects.create_user(username=username, password=password)
    user.save()

    player = Player.objects.create(user=user, location=start_location, gold=properties.STARTING_GOLD_VALUE)
    params = PlayableParameter.objects.all()
    for param in params:
        PlayerParameter.objects.create(player=player, parameter=param, state=param.default_val)

    attrs = AllAttribute.objects.all()
    for attr in attrs:
        PlayerAttribute.objects.create(player=player, attribute=attr, current_state=attr.default_val)

    player.regain_all_attributes()  #important!

    c = {'message': "Utworzono konto. Zaloguj się."}
    c.update(csrf(request))
    return render_to_response('index.html', c)


@is_user_authenticated_in_game
@is_user_active
def game(request):
    player = UserController.get_player_from_user(request.user)
    if player or request.user.is_superuser:
        return redirection_with_greeting_message(request)

    c = {"message": "Brak gracza dla wybranego użytkownika. Skontaktuj się z administratorem"}
    c.update(csrf(request))
    return render_to_response('index.html', c)

@is_ajax_request
def commandExe(request):
    from game_app.command.process.processing import ProcessCommand

    player = Player.objects.get(user=request.user)
    old_lvl = player.get_level()

    process_command = ProcessCommand(request)
    response = process_command.execute()

    player = Player.objects.get(user=request.user)  #do not remove - its player with new params

    #turns off html from input
    command = html.escape(request.GET['command'])

    c = {
        'status': response.status,
        'command': command#,
        #'map': response.get_map()
    }

    if player is not None:
        #get xp if monster was killed
        level = player.get_level()

        if old_lvl < level:
            for i in range(0, level - old_lvl):
                player.increase_lvl()
                response.message = response.get_message() + Highlighter.information(
                    "<br><br> Zdobyłeś nowy poziom. <br>Rozdziel uzyskane atrybuty!")

        attributes = player.get_all_attributes()
        attrs_array = {}
        for attr in attributes:
            attrs_array[attr.get_attr_id()] = attr.current_state

        attributes_max = player.get_all_attributes_with_mods()
        attrs_array_max = {}
        for attr in attributes_max:
            attrs_array_max[attr.get_attr_id()] = attr.max_state

        c['attributes'] = str(attrs_array)
        c['attributes_max'] = str(attrs_array_max)

        armor = ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(player)

        #xp and hp changes after killing monster
        c['experience'] = player.experience
        c['level'] = player.get_level()
        c['gold'] = player.gold
        c['gold_name'] = GOLD

        parameters = player.get_all_parameters()
        param_array = {}

        for param in parameters:
            param_array[param.get_param_id()] = param.state

        c['parameters'] = str(param_array)
        #item_param_list = player.get_item_param_list()
        #c['parametersItems'] = item_param_list
        c['armor'] = armor

    c['message'] = response.get_message()
    print(response.get_message())
    return HttpResponse(json.dumps(c), content_type='application/json')

@is_ajax_request
def report_response(request):
    player = UserController.get_player_from_user(request.user)
    response = request.GET['response']
    now = datetime.datetime.utcnow().replace(tzinfo=utc)

    report = ReportResponse(player=player, response=response, date=now)
    report.save()


@user_passes_test(lambda u: u.is_superuser)
def create(request):
    from game_app.world import create_world

    create_world.Creator().execute()
    return render_to_response('worldCreator.html')


@user_passes_test(lambda u: u.is_superuser)
def create_backup(request):
    results = create_backup.Backup().create_backup(request.POST.getlist('choosen_tables'))
    c = {'results': results}
    c.update(csrf(request))
    return render_to_response('create.html', c)


@user_passes_test(lambda u: u.is_superuser)
def restore_backup(request):
    results = restore_backup.RestoreBackup().execute(request.POST.getlist('choosen_tables'),
                                                     request.POST.get('directory'), request.POST.get('type'))
    c = {'results': results}
    c.update(csrf(request))
    return render_to_response('create.html', c)


@user_passes_test(lambda u: u.is_superuser)
def choose_backup(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('chooseBackup.html', c)


@user_passes_test(lambda u: u.is_superuser)
def choose_backup_restore(request):
    import os

    c = {}
    dir_names = []
    for dir_name in os.listdir(os.getcwd() + '/BACKUP'):
        if dir_name.startswith("backup_"):
            dir_names.append(dir_name)
    c['dir_names'] = dir_names
    c.update(csrf(request))
    return render_to_response('chooseBackupRestore.html', c)


@is_ajax_request
@user_passes_test(lambda u: u.is_superuser)
def get_backup_tables(request):
    #from game_app.world.backup.restore_backup import TableRequestParser

    #process_command = TableRequestParser(request)
    #table_names = process_command.process_command()

    #c = {'table_names': table_names, 'status': 'success'}
    return HttpResponse(json.dumps(c = {}), content_type='application/json')
