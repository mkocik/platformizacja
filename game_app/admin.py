from django.contrib import admin
from game_app.models import *
import sys
from Platformizacja.settings import logger

def is_abstract(cls):
    return bool(getattr(cls, "__abstractmethods__", False))

for cls in models.Model.__subclasses__():
    if cls not in admin.site._registry:
        try:
            admin.site.register(cls)
        except:
            logger.warning(sys.exc_info())


