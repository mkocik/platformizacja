# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.models.PlayerModel import User, Player


class UserController(object):
    @classmethod
    def get_player_from_user(cls, user):
        try:
            return Player.objects.get(user=user)
        except Player.DoesNotExist:
            return None

    @classmethod
    def get_user_from_user_name(cls, user_name):
        try:
            return User.objects.get(username=user_name)
        except User.DoesNotExist:
            return None

    @classmethod
    def get_all_users(cls):
        return User.objects.all()
