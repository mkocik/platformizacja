from django.core.context_processors import csrf
from django.http import HttpResponse
from django.shortcuts import render_to_response
from game_app.greeting_view import redirection_with_greeting_message
import json

''' WRAPPERS '''
def is_ajax_request(func):
    def func_wrapper(request):
        if request.is_ajax():
            return func(request)
        else:
            return HttpResponse(json.dumps({'status': 'error'}), content_type='application/json')
    return func_wrapper

def is_user_not_authenticated(func):
    def func_wrapper(request):
        if request.user.is_authenticated():
            return redirection_with_greeting_message(request)
        else:
            return func(request)
    return func_wrapper

def is_user_authenticated_in_game(func):
    def func_wrapper(request):
        if request.user.is_authenticated():
            return func(request)
        else:
            c = {'message': "Odmowa dostępu. Zaloguj się."}
            c.update(csrf(request))
            return render_to_response('index.html', c)
    return func_wrapper

def is_user_active(func):
    def func_wrapper(request):
        if request.user.is_active:
            return func(request)
    return func_wrapper

def is_request_method_post(func):
    def func_wrapper(request):
        if request.method == "POST":
            return  func(request)
        else:
            c = {}
            c.update(csrf(request))
            return render_to_response('register.html', c)
    return func_wrapper
''' END WRAPPERS '''