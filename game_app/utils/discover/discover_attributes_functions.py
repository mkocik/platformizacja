# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from MUD_ADMIN.game import attributes_functions

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

attributes_functions_dictionary = {}

for y in [getattr(attributes_functions, x) for x in dir(attributes_functions)]:
        if callable(y):
           attributes_functions_dictionary[y.__name__] = y

#print(attributes_functions_dictionary)
