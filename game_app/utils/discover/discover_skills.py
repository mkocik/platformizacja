# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from inspect import getmembers, isclass, getmro
import sys
from MUD_ADMIN.game.skills.skills import *

abstract_skills_classes = []

for name, class_object in getmembers(sys.modules['MUD_ADMIN.game.skills.abstract_skills']):

    if isclass(class_object):

        if AbstractSkill in getmro(class_object):
            abstract_skills_classes.append(class_object)


all_skills_class_dictionary = {}
skills_description_dictionary = {}

for name, class_object in getmembers(sys.modules[SimpleChestSkill.__module__]):

    if isclass(class_object) and class_object not in abstract_skills_classes:
        all_skills_class_dictionary[class_object.name] = class_object
        skills_description_dictionary[class_object.name] = class_object.description

#print("-----")
#print(skills_description_dictionary)
#print(all_skills_class_dictionary)
#print("-----")



