class ModuleManager(object):
    modules_mapper = {}
    commands = []

    def register_module(self, module, class_obj):
        if module not in self.modules_mapper:
            self.modules_mapper[module] = class_obj()
        else:
            raise Exception("Module " + module + " has been already registered")

    def get_module(self, interface):
        if interface in self.modules_mapper:
            return self.modules_mapper[interface]
        else:
            return None

    def get_all_modules(self):
        return self.modules_mapper

    def init_all_modules(self):
        import os
        import importlib.machinery

        lst = os.listdir("modules")
        #dir = []
        '''for d in lst:
            s = os.path.abspath("modules") + os.sep + d
            if os.path.isdir(s) and os.path.exists(s + os.sep + "__init__.py"):
                dir.append(d)
        # load the modules
        for d in dir:
            self.modules[d] = __import__("modules." + d, fromlist = ["*"])'''
        for d in lst:
            s = os.path.abspath("modules") + os.sep + d
            if os.path.isdir(s) and os.path.exists(s + os.sep + "__init__.py"):
                loader = importlib.machinery.SourceFileLoader(d, s + os.sep + "__init__.py")
                loader.load_module() #loads models


    def register_command(self, command):
        if command not in self.commands:
            self.commands.append(command)
        else:
            raise Exception("Command " + command + " has been already registered")


    def get_command_by_name(self, command_name):
        for item in self.commands:
            if item.name == command_name:
                return item
        return None

    def get_command_by_shortcut(self, shortcut):
        for item in self.commands:
            if item.shortcut == shortcut:
                return item
        return None

    def get_all_commands(self):
        return self.commands


module_manager = ModuleManager()
