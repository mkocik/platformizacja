# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime

from django.utils.timezone import utc

from game_app.controllers.basic_controllers import UserController
from game_app.models.PlayerModel import ReportTime, User

class WsHandler(object):
    def __init__(self):
        self.available_players = {}


    def register(self, user_name, web_socket):
        user = UserController.get_user_from_user_name(user_name)
        if user is None:
            return False

        player = UserController.get_player_from_user(user)
        if player is None:
            return False

        now = datetime.datetime.utcnow().replace(tzinfo=utc)
        self.available_players[user] = PlayerProperties(player, web_socket, now)

        return True

    def unregister(self, user_name):
        for user_in_list in self.available_players.keys():

            if user_in_list.username == user_name:

                player_properties = self.available_players[user_in_list]
                now = datetime.datetime.utcnow().replace(tzinfo=utc)
                report_time = ReportTime(player=player_properties.player, start_date=player_properties.start_date,
                                         end_date=now)
                report_time.save()

                del self.available_players[user_in_list]

                return True

        return False

    def is_player_connected(self, user):
        for user_in_list in self.available_players.keys():
            if user_in_list.username == user.username:
                return True
        return False

    def say_to_users(self, users, msg):
        for user in users:
            if type(user) is User:
                if user in self.available_players.keys():
                    web_socket = self.available_players[user].web_socket
                    web_socket.write_message(msg)

    def say_to_user(self, user, msg):
        self.say_to_users([user], msg)


class PlayerProperties:
    def __init__(self, player, web_socket, start_date):
        self.player = player
        self.web_socket = web_socket
        self.start_date = start_date

    def get_web_socket(self):
        return self.web_socket



ws_handler = WsHandler()