from django.shortcuts import render_to_response
from Platformizacja.settings import logger
from MUD_ADMIN.game import properties
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics
from game_app.controllers.basic_controllers import UserController
from game_app.models.PlayerModel import PlayerParameter
from language.PL import *


def redirection_with_greeting_message(request):
    if request.user.is_superuser:
        c = {'superuser': request.user.is_superuser}

    else:
        #from game_app.command.location import MoveController, LocationController
        #from game_app.command.settings.command_settings import get_special_attack_time
        from game_app.utils.ModuleManager import module_manager

        # if player game was interrupted program clears all activities:
        # for example battles and talks with NPC
        player = UserController.get_player_from_user(request.user)

        parameter_list = PlayerParameter.objects.filter(player=player)
        location = player.location

        attributes_list = player.get_all_attributes_with_mods()

        greeting_message = "Witaj %s." % request.user.username
        #moving_controller = MoveController()
        #location_message = moving_controller.get_full_description(location, request.user)

        location_message = module_manager.get_module("MapModule").get_full_field_description(location, request.user)

        map_fields = module_manager.get_module("MapModule").get_map_fields(location)

        c = {'nick': request.user.username,
             'paramlist': parameter_list,
             'attrList': attributes_list,
             'greetingMessage': greeting_message,
             'locationMessage': location_message,
             'timeout': properties.COMMAND_TIMEOUT,

             'experience': player.experience,
             'level': player.get_level(),
             'mapFields': map_fields,
             'gold': player.gold,
             'gold_name': GOLD,
             'armor': ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(player),
        }

    return render_to_response('game.html', c)