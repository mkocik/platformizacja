from game_app.utils.ModuleManager import module_manager
from .module_init import ItemModuleInit
from .settings.command_settings import *


''' register module '''
if "ItemModule" not in module_manager.get_all_modules():
    module_manager.register_module("ItemModule", ItemModuleInit)


''' register commands '''
if gett not in module_manager.get_all_commands():
    module_manager.register_command(gett)

if remove not in module_manager.get_all_commands():
    module_manager.register_command(remove)

if inventory not in module_manager.get_all_commands():
    module_manager.register_command(inventory)

if inn not in module_manager.get_all_commands():
    module_manager.register_command(inn)

if out not in module_manager.get_all_commands():
    module_manager.register_command(out)

if character not in module_manager.get_all_commands():
    module_manager.register_command(character)