from ..models.ItemModel import *

class LocationController(object):
    def __init__(self, location):
        self.location = location

    def get_items(self):
        try:
            items = ItemInMap.objects.filter(map=self.location)
            return items
        except ItemInMap.DoesNotExist:
            return None

    def remove_item(self, item):
        item.delete()


    def get_single_item(self):
        try:
            item_in_map = ItemInMap.objects.get(map=self.location)
            return item_in_map
        except:
            return None

    #get first of item with given name from current field
    def get_item(self, name):
        try:
            item = Item.objects.get(name=name)
            item_in_map = ItemInMap.objects.filter(map=self.location, item=item)
            return item_in_map[0]
        except:
            return None

    def get_item_by_id_from_dict(self, number, amount=1):
        try:
            items = self.get_items_in_map_as_dict()

            if number - 1 >= len(items):
                return None
            counter = 1

            for it in items:
                if counter == number:

                    if amount > items[it]:
                        return False
                    return it

                counter += 1

            return None
        except:
            return None

    def get_items_by_name(self, name, amount, player):
        item_in_map = ItemInMap.objects.filter(map=self.location)
        for item in item_in_map:
            if item.item.name == name:
                if amount > 0:
                    player.add_to_equipment(item.item)
                    self.remove_item(item)
                    amount -= 1

    def get_items_in_map_as_dict(self):
        try:
            item_in_map = ItemInMap.objects.filter(map=self.location)
            dict = {}
            items_name = []

            for item in item_in_map:
                if item.item.name in items_name:

                    for key, value in dict.items():
                        if key.item.name == item.item.name:
                            dict[key] += 1

                else:
                    items_name.append(item.item.name)
                    dict[item] = 1

        except:
            dict = None
        return dict

    def get_item_probability_for_land_type(self):
        try:
            item_prob = ItemOnSpecificLand.objects.get(map=self.location.land_type)
            return item_prob.probability
        except:
            return 0

    def get_item_land_types(self):
        return ItemLandType.objects.filter(land_type=self.location.land_type)

    def choose_random_item_from_location(self):
        import random

        items_land_type = self.get_item_land_types()
        items_land_type_available = []

        for item_land_type in items_land_type:
            for probability in range(item_land_type.probability):
                items_land_type_available.append(item_land_type)

        #if no item for this land complex
        if len(items_land_type_available) == 0:
            return None

        item_land_type = random.choice(items_land_type_available)
        item = item_land_type.item
        item_in_map = self.create_item(item)

        return item_in_map

    def create_item(self, item):
        item_in_map = ItemInMap(map=self.location, item=item)
        item_in_map.save()
        return item_in_map

    def roll_item_on_map(self):
        import random

        if len(self.location.get_players()) > 1:  #if there is a player on the current field
            return

        random_int = random.randint(0, 100)

        try:
            mapModuleObject = module_manager.get_module("MapModule")
            start_location_id = mapModuleObject.get_starting_location().pk

        except:
            start_location_id = 0

        if random_int <= self.get_item_probability_for_land_type() and self.location.pk != start_location_id:
            item_in_map = self.choose_random_item_from_location()


    def get_item_description(self):
        from game_app.command.highlighter import Highlighter

        items_in_map = self.get_items_in_map_as_dict()

        if items_in_map is not None and items_in_map:

            message = "<br>Znalazłeś "
            item_counter = 1

            for item_in_map, amount in items_in_map.items():

                if amount == 1:
                    message = message + (Highlighter.bold(str(item_counter) + ". ")) + Highlighter.information(
                        item_in_map.item.name) + ", "

                else:
                    message = message + (Highlighter.bold(str(item_counter) + ". ") + Highlighter.information(
                        item_in_map.item.name)) + " (" + str(amount) + "), "

                item_counter += 1

            message = message[:-2]
            return message
        else:
            return ""
