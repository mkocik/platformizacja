from ..models.ItemModel import ItemInMap,PlayerEquipment,PlayerEquipmentSlot,EquipmentSlot

class ItemController(object):
    def __init__(self, player):
        self.player = player

    def get_inventory_amount(self):
        try:
            equipment = PlayerEquipment.objects.filter(player=self.player)
            return len(equipment)
        except:
            return 0

    def get_all_inventory(self):
        try:
            equipment = PlayerEquipment.objects.filter(player=self.player)
            return equipment
        except:
            return None

    def get_all_inventory_as_dict(self):
        try:
            equipment = PlayerEquipment.objects.filter(player=self.player)

            dict = {}
            eq_name = []

            for eq in equipment:
                if eq.item.name in eq_name:
                    for key, value in dict.items():
                        if key.item.name == eq.item.name:
                            dict[key] += 1
                else:
                    eq_name.append(eq.item.name)
                    dict[eq] = 1
        except:
            dict = None
        return dict

    def increase_gold(self, amount):
        self.player.gold = self.player.gold + amount
        self.player.save()

    def get_all_equipped_slots(self):
        try:
            eq_items = PlayerEquipmentSlot.objects.filter(player=self.player)
            return eq_items
        except:
            return None

    def get_gold_from_map(self):
        from ...MapModule.models.MapModel import GoldInMap

        try:
            gold = GoldInMap.objects.get(map=self.player.location)
            amount = gold.amount
            self.increase_gold(amount)
            gold.delete()
            return amount
        except:
            return None

    def equip_item(self, item):
        try:
            eq_del = PlayerEquipment.objects.filter(player=self.player, item=item)
            eq = PlayerEquipmentSlot(player=self.player, item=item)
            eq.save()
            eq_del[0].delete()
        except:
            return None

    def get_item_from_inventory_by_number(self, number):
        try:
            player_equipment = self.get_all_inventory_as_dict()
            items = [item for item in player_equipment.keys()]
            if number - 1 >= len(items):
                return None
            return items[number - 1]
        except:
            return None


    def remove_from_equipped_slots_by_index(self, index):
        try:
            eq_items = PlayerEquipmentSlot.objects.filter(player=self.player)
            if index <= 0 or index > len(eq_items):
                return False
            eq = PlayerEquipment(player=self.player, item=eq_items[index - 1].item)
            eq.save()
            slot_to_delete = eq_items[index - 1]
            slot_to_delete.delete()
            return True
        except:
            return None

    def is_slot_empty(self, item):

        if item.item_type.equipment_slot is None:
            return None

        equipment_slots = EquipmentSlot.objects.all()
        all_eq_slots = {}

        for eq in equipment_slots:
            all_eq_slots[eq.name] = eq.multiplicity

        try:
            player_equipment_slots = PlayerEquipmentSlot.objects.filter(player=self.player)
            for p_eq in player_equipment_slots:
                all_eq_slots[p_eq.item.item_type.equipment_slot.name] -= 1

            if all_eq_slots[item.item_type.equipment_slot.name] > 0:
                return True
            else:
                return False
        except PlayerEquipmentSlot.DoesNotExist:
            return True

    def get_equipment_description(self, asset):
        from language.PL import GOLD
        #from game_app.command.execution.mode.trade import TradePriceRates
        equipment_quantity = self.get_all_inventory_as_dict()

        message = "Posiadasz: " + GOLD + " " + str(self.player.gold) + "<br/>"

        if len(equipment_quantity.keys()) > 0:
            message += "<br> Posiadasz przedmioty: <br>"
        else:
            message += "<br> Nie posiadasz przedmiotów. <br>"
            return message

        index = 1

        for equipment, quantity in equipment_quantity.items():

            #item_price = TradePriceRates().get_price(equipment.item.value, asset, self)
            message += "%s. %s (%s) -- %s <br>" % (index, equipment.item.name, quantity, equipment.item.value)
            index += 1

        return message + '<br>'

    def delete_item_from_inventory_by_number(self, number, amount=1):
        try:
            items = self.get_all_inventory_as_dict()
            counter = 1

            for key, value in items.items():

                if counter == number:

                    if value < amount:
                        return None

                    name = key.item.name
                    all_eq = PlayerEquipment.objects.filter(player=self.player)

                    for eq_item in all_eq:

                        if eq_item.item.name == name:

                            if amount > 0:
                                item = ItemInMap(map=self.player.location, item=eq_item.item)
                                item.save()

                                eq_item.delete()
                                amount -= 1
                    return True

                counter += counter

            return None

        except:
            return None

    def add_to_equipment(self, item):
        equipment = PlayerEquipment(player=self.player, item=item)
        equipment.save()