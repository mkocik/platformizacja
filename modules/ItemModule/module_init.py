
from interfaces.module import Module
from interfaces.randomly_set_on_map import RandomlySetOnMap
from .world.create_items import *
from .command.LocationController import LocationController

class ItemModuleInit(Module, RandomlySetOnMap):
    def set_on_map(self, location):
        return LocationController(location).roll_item_on_map()

    def get_all_desc_in_location(self, location):
        return LocationController(location).get_item_description()

    def fire_entry_event_in_location(self, player):
        pass

    def create_world(self):
        items_creator_path = "modules/ItemModule/world/creator/world_creator/"

        CreateEquipmentSlot().execute(items_creator_path)
        CreateItemType().execute(items_creator_path)
        CreateItem().execute(items_creator_path)
        CreateItemLandType().execute(items_creator_path)
        CreateItemParameter().execute(items_creator_path)
        CreateItemInMap().execute(items_creator_path)

    def return_path_to_model(self):
        return 'modules/MapModule/models/ItemModel.py'

