# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import codecs
import os

from ..models.ItemModel import *
from game_app.world.create_world import ICreator
from modules.MapModule.models.MapModel import *


class CreateItemType(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:

            try:
                eq_slot = EquipmentSlot.objects.get(pk=words[1])
            except EquipmentSlot.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany EquipmentSlot nie istnieje")
                return -2

            item = ItemType(name=words[0], equipment_slot=eq_slot)
            item.save()

        elif len(words) == 1:

            item = ItemType(name=words[0])
            item.save()

        else:
            print("Omienięto niepełny wiersz w ItemType.csv")

    def execute(self, path):

        ItemType.objects.all().delete()

        file_name = "ItemType.csv"
        ICreator.create_world(self,  file_name, path)

        return 0

class CreateEquipmentSlot(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            eq_slot = EquipmentSlot(name=words[0], multiplicity=words[1])
            eq_slot.save()
        else:
            print("Omienięto niepełny wiersz w " + file_name)

    def execute(self, path):

        EquipmentSlot.objects.all().delete()

        file_name = "EquipmentSlot.csv"
        ICreator.create_world(self, file_name, path)

        return 0

class CreateItem(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 4:
            try:
                item_t = ItemType.objects.get(pk=words[0])
            except ItemType.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany typ przedmiotu nie istnieje")
                return -2

            item = Item(item_type=item_t, name=words[1], description=words[2], value=words[3])
            item.save()
        else:
            print("Omienięto pusty wiersz w " + file_name)

    def execute(self, path):

        Item.objects.all().delete()

        file_name = "Item.csv"
        ICreator.create_world(self, file_name, path)

        return 0


class CreateItemLandType(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            try:
                item = Item.objects.get(pk=words[0])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            try:
                land_type = LandType.objects.get(pk=words[1])
            except LandType.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany typ terenu nie istnieje")
                return -2

            item_land_type = ItemLandType(item=item, land_type=land_type, probability=words[2])
            item_land_type.save()

    def execute(self, path):

        file_name = "ItemLandType.csv"
        ICreator.create_world(self, file_name, path)

        return 0


class CreateItemParameter(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:

            try:
                item_t = Item.objects.get(pk=words[0])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            try:
                param = PlayableParameter.objects.get(pk=words[1])
            except PlayableParameter.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany parametr nie istnieje")
                return -2

            parameter = ItemParameter(item=item_t, parameter=param, state=words[2])
            parameter.save()

        elif len(words) == 2:
            #if parameter==None then it is armor
            try:
                item_t = Item.objects.get(pk=words[0])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            parameter = ItemParameter(item=item_t, state=words[1])
            parameter.save()

    def execute(self, path):

        ItemParameter.objects.all().delete()

        file_name = "ItemParameter.csv"
        ICreator.create_world(self, file_name, path)

        return 0


class CreateItemInMap(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            try:
                map = Map.objects.get(pk=words[0])
            except Map.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane pole nie istnieje")
                return -2

            try:
                item = Item.objects.get(pk=words[1])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            item_in_map = ItemInMap(map=map, item=item)
            item_in_map.save()

    def execute(self, path):

        ItemInMap.objects.all().delete()

        file_name = "ItemInMap.csv"
        ICreator.create_world(self, file_name, path)

        return 0
