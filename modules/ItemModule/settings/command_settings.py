from .command_types import *
#from game_app.utils.ModuleManager import module_manager
from game_app.command.settings.command_definition import *
from ..command.execution.items import GetCommand,InventoryCommand,RemoveCommand,CharacterCommand,InCommand,OutCommand
from language.PL import *

#############################
# Item commands
#

gett = CommandProperties("weź",
                         "zabiera wybrany przedmiot z pola (weź nazwa_przedmiotu/numer [ilość_do_wzięcia] " +
                         "lub weź wszystko lub weź " + GOLD.lower() + ")", GetCommand, ItemTypeCommand, "get", order=1)
remove = CommandProperties("wyrzuć",
                           "wyrzuca wybrany przedmiot z ekwipunku (wyrzuć numer_przedmiotu [ilość_do_wyrzucenia])",
                           RemoveCommand, ItemTypeCommand, "r", order=2)
inventory = CommandProperties("ekwipunek",
                              "pozwala przejrzeć ekwipunek (i) lub zobaczyć opis przedmiotu (i numer_przedmiotu)",
                              InventoryCommand, ItemTypeCommand, "i", order=3)

character = CommandProperties("postać", "wyswietla noszone przedmioty", CharacterCommand, ItemTypeCommand, "c",
                              only_one_argument=True, order=5)

#in is a reserved word in python
inn = CommandProperties("włóż", "zakłada wybrany przedmiot (włóż numer_przedmiotu)", InCommand, ItemTypeCommand, "in",
                        order=6)
out = CommandProperties("zdejmij", "zdejmuje wybrany przedmiot (zdejmij numer_przedmiotu)", OutCommand, ItemTypeCommand,
                        "out", order=7)
