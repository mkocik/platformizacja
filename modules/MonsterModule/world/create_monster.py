# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from ..models.MonsterModel import *
from game_app.models.PlayerModel import *
from game_app.world.create_world import ICreator
from modules.MapModule.models.MapModel import *

class CreateMonster(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) in [9, 10]:

            monster = Monster(name=words[0], description=words[1], gold_min=words[2], gold_max=words[3],
                               experience=words[4], default_hp=words[5], aggression=words[6],
                               max_loot_amount=words[7], item_probability=words[8])

            monster.save()

            parameters = PlayableParameter.objects.all()
            for parameter in parameters:
                param_monster = MonsterParameter(monster=monster, parameter=parameter, state=parameter.default_val)
                param_monster.save()


    def execute(self, path):
        MonsterParameter.objects.all().delete()
        MonsterInMap.objects.all().delete()
        Monster.objects.all().delete()
        MonsterLandType.objects.all().delete()

        file_name = "Monster.csv"
        ICreator.create_world(self, file_name, path)

        return 0


class CreateMonsterParameters(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            try:
                monster = Monster.objects.get(pk=words[0])
            except Monster.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany potwor nie istnieje")
                return -2

            try:
                parameter = PlayableParameter.objects.get(pk=words[1])
            except PlayableParameter.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany parametr nie istnieje")
                return -2

            monster_params = MonsterParameter.objects.get(monster=monster, parameter=parameter)
            monster_params.state = words[2]
            monster_params.save()

    def execute(self, path):

        file_name = "MonsterParameter.csv"
        ICreator.create_world(self, file_name, path)

        return 0


class CreateMonsterLandTypes(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            try:
                monster = Monster.objects.get(pk=words[0])
            except Monster.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany potwor nie istnieje")
                return -2

            try:
                land_type = LandType.objects.get(pk=words[1])
            except LandType.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany typ terenu nie istnieje")
                return -2

            monster_land_type = MonsterLandType(monster=monster, land_type=land_type, probability=words[2])
            monster_land_type.save()

    def execute(self, path):

        file_name = "MonsterLandType.csv"
        ICreator.create_world(self, file_name, path)

        return 0


class CreateMonsterInMap(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            try:
                monster = Monster.objects.get(pk=words[0])
            except Monster.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany potwor nie istnieje")
                return -2

            try:
                map = Map.objects.get(pk=words[1])
            except Map.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane pole nie istnieje")
                return -2

            if map != Map.objects.get(x=properties.START_LOCATION_X, y=properties.START_LOCATION_Y,
                                      level=MapLevel.objects.get(level=properties.START_LOCATION_LVL)):
                monster_in_map = MonsterInMap(map=map, monster=monster)
                monster_in_map.save()

    def execute(self, path):

        file_name = "MonsterInMap.csv"
        ICreator.create_world(self, file_name, path)

        return 0
