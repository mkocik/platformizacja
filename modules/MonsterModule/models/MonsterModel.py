# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import MinValueValidator



class Monster(models.Model):
    class Meta:
        app_label = "game_app"

    name = models.CharField(max_length=50, null=False)
    description = models.TextField()
    default_hp = models.IntegerField(null=False)
    gold_min = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    gold_max = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    experience = models.IntegerField(default=1)
    aggression = models.IntegerField(default=50)
    max_loot_amount = models.IntegerField(default=1)
    item_probability = models.IntegerField(default=50)
    special_attack_probability = models.IntegerField(default=50)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


    def get_parameter(self, name):
        from game_app.models import PlayableParameter

        parameter = PlayableParameter.objects.get(name=name)
        parameter_value = MonsterParameter.objects.get(monster=self, parameter=parameter).state
        return parameter_value

    def set_parameter(self, parameter_name, state):
        from game_app.models import PlayableParameter

        try:
            parameter = PlayableParameter.objects.get(name=parameter_name)
            player_parameter = MonsterParameter.objects.get(monster=self, parameter=parameter)
            player_parameter.state = state
            player_parameter.save()
        except:
            return


class MonsterLandType(models.Model):
    class Meta: app_label = "game_app"

    monster = models.ForeignKey("Monster", null=False)
    land_type = models.ForeignKey("LandType", null=False)
    probability = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.monster.__str__() + " - " + self.land_type.__str__()


class MonsterParameter(models.Model):
    class Meta: app_label = "game_app"

    monster = models.ForeignKey('Monster', null=False)
    parameter = models.ForeignKey('PlayableParameter', blank=True, null=True, default=None)
    state = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.monster.__str__() + " - " + self.parameter.__str__()


class MonsterInMap(models.Model):
    class Meta:
        app_label = "game_app"

    map = models.ForeignKey('Map', null=False)
    monster = models.ForeignKey('Monster', null=False)
    player = models.ForeignKey('Player', blank=True, null=True, default=None)
    hp = models.IntegerField(null=True)

    #settings default value for hp from other table
    def __init__(self, *args, **kwargs):
        monster = kwargs.get('monster')
        super(MonsterInMap, self).__init__(*args, **kwargs)
        if monster is not None:
            self.hp = self.monster.default_hp

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return "(" + str(self.map.x) + ", " + str(self.map.y) + "lvl: " + str(
            self.map.level) + ") -> " + self.monster.name

    def set_player(self, player):
        self.player = player
        self.save()

    def reduce_hp(self, difference):
        self.hp -= difference
        self.save()
        return self.hp

    #for tests
    def set_hp(self, hp):
        self.hp = hp
        self.save()

    def get_hp(self):
        return self.hp

    def get_ws_id(self):
        return self.__str__()

    def get_clear_parameter(self):
        return 0

    def resurrection(self):
        # from game_app.utils.websockets.battle_websocket import ws_handler
        #
        # if self.player is not None:
        #     ws_handler.stop_all_effects(self.player)
        #     ws_handler.stop_all_effects(self)

        MonsterInMap.objects.get(pk=self.pk).delete()


class MonsterOnSpecificLand(models.Model):
    class Meta:
        app_label = "game_app"

    map = models.ForeignKey('LandType', null=False)
    probability = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.probability.__str__() + " - " + self.map.__str__()