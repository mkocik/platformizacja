from game_app.utils.ModuleManager import module_manager
from .module_init import MonsterModuleInit


''' register module '''
if "MonsterModule" not in module_manager.get_all_modules():
    module_manager.register_module("MonsterModule", MonsterModuleInit)