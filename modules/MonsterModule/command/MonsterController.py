from ..models.MonsterModel import MonsterInMap,MonsterLandType,MonsterOnSpecificLand
from game_app.command.highlighter import Highlighter
import random

class MonsterController(object):
    def __init__(self, location):
        self.location = location

    def get_monster_in_map(self):
        try:
            monster_in_maps = MonsterInMap.objects.filter(map=self.location)

            if monster_in_maps.count() == 1:
                return MonsterInMap.objects.get(map=self.location)
            else:
                return None

        except MonsterInMap.DoesNotExist:
            return None

    def roll_random_monster(self):

        monster_in_map = self.get_monster_in_map()

        if monster_in_map:
            return None
        else:
            if len(self.location.get_players()) > 1:  #if there is a player on the current field
                return ""

            random_int = random.randint(0, 100)

            #we cannot meet monster in starting map
            if random_int <= self.get_monster_probability_for_land_type():

                monster_in_map = self.choose_random_monster_from_location()
                if monster_in_map is None:
                    return ""
                monster = monster_in_map.monster
            else:
                return ""

    def get_monster_probability_for_land_type(self):
        try:
            monster_prob = MonsterOnSpecificLand.objects.get(map=self.location.land_type)
            return monster_prob.probability
        except:
            return 0

    def get_monster_land_types(self):
        return MonsterLandType.objects.filter(land_type=self.location.land_type)

    def create_monster(self, monster):
        monster_in_map = MonsterInMap(map=self.location, monster=monster)
        monster_in_map.save()
        return monster_in_map

    def get_monster_description(self):
        monster_in_map = self.get_monster_in_map()

        if monster_in_map:
            monster = monster_in_map.monster

            if monster_in_map.player is not None:
                monster_name = Highlighter.bold(monster.name)
                return "Na tym polu znajduje się %s, który walczy obecnie z innych graczem." % monster_name

            introduction = "<br>" + Highlighter.monster_description("Spotkałeś potwora ")
            monster_name = Highlighter.monster_name(monster.name + ": ")
            monster_description = Highlighter.monster_description(monster.description)

            message = introduction + monster_name + "<br>" + monster_description

            return message
        else:
            return ""

    def choose_random_monster_from_location(self):

        monsters_land_type = self.get_monster_land_types()
        monsters_land_type_available = []

        for monster_land_type in monsters_land_type:
            for probability in range(monster_land_type.probability):
                monsters_land_type_available.append(monster_land_type)

        #if no monster for this land complex
        if len(monsters_land_type_available) == 0:
            return None

        monster_land_type = random.choice(monsters_land_type_available)
        monster = monster_land_type.monster
        monster_in_map = self.create_monster(monster)

        return monster_in_map
