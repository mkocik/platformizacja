from .world.create_monster import CreateMonster,CreateMonsterInMap,CreateMonsterLandTypes,CreateMonsterParameters
from interfaces.module import Module
from interfaces.randomly_set_on_map import RandomlySetOnMap
from interfaces.living_creature import LivingCreature
from .command.MonsterController import MonsterController

class MonsterModuleInit(Module, RandomlySetOnMap):
    def set_on_map(self, location):
        return MonsterController(location).roll_random_monster()

    def get_all_desc_in_location(self, location):
        return MonsterController(location).get_monster_description()

    def fire_entry_event_in_location(self, player):
        pass

    def create_world(self):
        monsters_creator_path = "modules/MonsterModule/world/creator/world_creator/"

        CreateMonster().execute(monsters_creator_path)
        CreateMonsterParameters().execute(monsters_creator_path)
        CreateMonsterLandTypes().execute(monsters_creator_path)
        CreateMonsterInMap().execute(monsters_creator_path)

    def return_path_to_model(self):
        return 'modules/MonsterModule/models/MonsterModel.py'

