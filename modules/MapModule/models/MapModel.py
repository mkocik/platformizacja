# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
#to synchronise db: python manage.py syncdb

class LandType(models.Model):
    class Meta: app_label = "game_app"

    land_name = models.CharField(max_length=20, null=False)
    #monster_probability = models.IntegerField(null=False)
    #item_probability = models.IntegerField(null=False)
    default_description = models.TextField(default="")
    number = models.IntegerField(null=False, unique=True)

    def __str__(self):
        return self.land_name

    def __unicode__(self):
        return self.__str__()

    '''def set_monster_probability(self, percent):
        self.monster_probability = percent
        self.save()

    def set_item_probability(self, percent):
        self.item_probability = percent
        self.save()'''


class MapLevel(models.Model):
    class Meta:
        app_label = "game_app"

    level = models.IntegerField(null=False, default=0)
    name = models.CharField(null=False, max_length=20)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return 'level ' + str(self.level) + ': ' + self.name


class Map(models.Model):
    class Meta:
        app_label = "game_app"
        ordering = ('level', 'x', 'y',)

    x = models.IntegerField(null=False)
    y = models.IntegerField(null=False)
    land_type = models.ForeignKey("LandType", null=False)
    description = models.TextField()
    #level = models.IntegerField(null=False, default=0)
    level = models.ForeignKey("MapLevel", null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return "(%s, %s, %s)" % (self.x, self.y, self.level.level)

    # @transaction.atomic
    def drop_gold(self, amount):
        try:
            # with transaction.atomic():
                gold_in_map = GoldInMap.objects.get(map=self)
                gold = gold_in_map.amount
                gold_in_map.amount = amount + gold
                gold_in_map.save()
        # except IntegrityError:
        #     self.drop_gold(amount)
        except GoldInMap.DoesNotExist:
            gold_in_map = GoldInMap(map=self, amount=amount)
            gold_in_map.save()

    def get_gold_in_map(self):
        try:
            gold_in_map = GoldInMap.objects.get(map=self)
            return gold_in_map.amount
        except GoldInMap.DoesNotExist:
            return 0

    def is_direction_available(self, curr_id, coord_x, coord_y):
        try:
            map_obj = Map.objects.get(pk=curr_id)
            return Map.objects.get(x=map_obj.x + coord_x, y=map_obj.y + coord_y, level=map_obj.level)
        except Map.DoesNotExist:
            return None

    def get_passages(self):
        try:
            passages = MapPassage.objects.filter(from_map=self)
            return passages
        except MapPassage.DoesNotExist:
            return None

    def is_passage(self):
        try:
            passages = MapPassage.objects.filter(from_map=self).count()
            return passages > 0
        except MapPassage.DoesNotExist:
            return False

    def get_surroundings(self):
        location = self
        surroundings = {}

        from ..settings.command_settings import direction_coordinates

        for offsetX, offsetY, directionName in direction_coordinates:
            if self.is_direction_available(location.pk, offsetX, offsetY):
                surroundings[directionName] = Map.objects.get(x=location.x + offsetX, y=location.y + offsetY,
                                                              level=location.level)
        return surroundings

    def get_players(self):
        from game_app.models.PlayerModel import Player

        players = Player.objects.all()
        players_in_location = []

        for player in players:
            if player.location == self:
                players_in_location.append(player)

        return players_in_location

    def get_passage(self):
        try:
            map_passage = MapPassage.objects.filter(from_map=self)
            return map_passage
        except:
            return None



class MapPassage(models.Model):
    class Meta: app_label = "game_app"

    name = models.CharField(null=False, max_length=50)
    from_map = models.ForeignKey('Map', related_name='from_map', null=False)
    to_map = models.ForeignKey('Map', related_name='to_map', null=False)
    #asset = models.ForeignKey('Riddle', null=True, blank=True)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        string_representation = '%s from %s to %s' % (self.name, self.from_map.__str__(), self.to_map.__str__())

        return string_representation

    def get_map(self):
        return self.from_map


class GoldInMap(models.Model):
    class Meta: app_label = "game_app"

    map = models.ForeignKey('Map', null=False)
    amount = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return "(" + str(self.map.x) + ", " + str(self.map.y) + ") -> " + str(self.amount)