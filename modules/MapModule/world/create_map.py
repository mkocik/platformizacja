# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import codecs
import os

from ..models.MapModel import Map, LandType, MapLevel, MapPassage
from game_app.world.create_world import ICreator


class CreateMap(ICreator):
    def execute(self):

        #map_creator_path = "MUD_ADMIN/creator/map_creator"
        map_creator_path = "modules/MapModule/world/creator/map_creator"

        CreateLandTypes().execute()

        while Map.objects.count():
            Map.objects.all()[0].delete()

        MapLevel.objects.all().delete()

        for file_name in os.listdir(os.getcwd() + '/' + map_creator_path):
            #if 2==2:
            if file_name.endswith(".csv"):
                try:
                    print(file_name)

                    file_name_without_extension = file_name.split(".")[0]
                    level = file_name_without_extension.split("_")[-1]

                    level = MapLevel(level=level, name=level)
                    level.save()


                    f = codecs.open(map_creator_path + '/' + file_name, 'r', encoding='utf-8')
                    print("Otworzono " + map_creator_path + file_name)

                    rows = f.read().split("\n")
                    height = 0

                    for height in range(0, len(rows)):
                        row = rows[height]

                        if row != "":

                            words = row.split(";")
                            words[-1] = words[-1].rstrip()

                            for width in range(0, len(words)):

                                try:
                                    if words[width] != "" and words[width] != "-1":
                                        land_type = LandType.objects.get(number=words[width])
                                        map = Map(x=width, y=height, land_type=land_type,
                                                  description=land_type.default_description, level=level)
                                        map.save()

                                except LandType.DoesNotExist:
                                    print("Ominięto zapis w " + file_name + " -> podany typ terenu nie istnieje")
                        else:
                            print("Omienięto niepełny wiersz w " + file_name)

                except IOError:
                    print('error')
                    pass

            CreateMapPassage().execute()


class CreateLandTypes(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            landType = LandType(land_name=words[0], #monster_probability=words[1], item_probability=words[2],
                                 default_description=words[1], number=words[2])
            landType.save()

    def execute(self):

        while LandType.objects.count():
            LandType.objects.all()[0].delete()

        file_name = "LandType.csv"
        ICreator.create_world(self, file_name, 'modules/MapModule/world/creator/world_creator/')

        return 0


class CreateMapPassage(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            try:
                from_map = Map.objects.get(pk=words[1])
            except Map.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane pole nie istnieje")
                return -2
            try:
                to_map = Map.objects.get(pk=words[2])
            except Map.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane pole nie istnieje")
                return -2

            passage = MapPassage(name=words[0], from_map=from_map, to_map=to_map)
            passage.save()

    def execute(self):

        MapPassage.objects.all().delete()

        file_name = "MapPassage.csv"
        ICreator.create_world(self, file_name)

        return 0
