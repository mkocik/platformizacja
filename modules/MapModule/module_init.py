from .models.MapModel import *
from .properties import *
from interfaces.module import Module

class MapModuleInit(Module):
    def get_starting_location(self):
        return Map.objects.get(x=START_LOCATION_X, y=START_LOCATION_Y,
                                         level=MapLevel.objects.get(level=START_LOCATION_LVL))

    def get_directions_names(self):
        from .settings.command_settings import north,east,south,west
        return [north.name, east.name, south.name, west.name]

    def move_command(self, request, command_name):
        from .command.execution.moving import MoveCommand
        from .settings.command_settings import translate_polish_direction_to_english

        return MoveCommand(request, translate_polish_direction_to_english[command_name])

    def get_full_field_description(self,location, player):
        from .command.location import MoveController

        return MoveController().get_full_description(location, player)

    def get_map_fields(self, location):
        from .command.location import LocationController

        return LocationController().get_map_fields(location)

    def return_path_to_model(self):
        return 'modules/MapModule/models/MapModel.py'

