# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.command.highlighter import Highlighter
from language.PL import *
from ..models.MapModel import *

class LocationController():
    @staticmethod
    def _get_location_message(location):
        message = 'Twoja obecna lokalizacja to (%s,%s,%s).' % (str(location.x), str(location.y), location.level.name)
        message += " Rodzaj terenu: %s." % location.land_type.land_name

        if location.get_passage() is not None and len(location.get_passage()) != 0:
            message += "<br>Możliwe przejścia z tego pola: "

            for passage in location.get_passage():
                message = message + "<b>" + passage.name + "</b>, "

            message = message[:-2]

        message += "<br>"
        return Highlighter.location(message)


    @staticmethod
    def _get_description_message(location):
        message = location.description
        return Highlighter.description(message)

    @staticmethod
    def _get_surroundings_message(location):
        surroundings = location.get_surroundings()

        message = "Możesz poruszyć się na: "

        from ..settings.command_settings import translate_english_direction_to_polish

        for directionName in surroundings.keys():
            message = message + translate_english_direction_to_polish[directionName] + ", "

        if message.endswith(', '):
            message = message[:-2]
            message += "."

        return Highlighter.surroundings(message)

    @staticmethod
    def get_partial_description(location):
        location_message = LocationController._get_location_message(location)
        description_message = LocationController._get_description_message(location)
        surroundings_message = LocationController._get_surroundings_message(location)

        message = location_message + '<br>' + description_message + '<br>' + surroundings_message

        return message

    @staticmethod
    def get_map_fields(location):
        level = location.level

        #size of the map
        difference = 3

        min_x = location.x - difference
        max_x = location.x + difference + 1
        min_y = location.y - difference
        max_y = location.y + difference + 1

        # Creates 2D list
        map_array = [[None for x in range(min_x, max_x)] for x in range(min_y, max_y)]

        for x in range(min_x, max_x):
            for y in range(min_y, max_y):

                try:
                    map = Map.objects.get(x=x, y=y, level=level)
                except Map.DoesNotExist:
                    map = None

                x_coor = x - min_x
                y_coor = y - min_y

                if map:
                    land_name = map.land_type.land_name

                    passage = map.is_passage()

                    user = (x == location.x) and (y == location.y)

                    map_array[y_coor][x_coor] = {"y": y_coor, "x": x_coor, "land_name": land_name, "user": user,
                                                 "passage": passage}

                else:
                    map_array[y_coor][x_coor] = {"y": y_coor, "x": x_coor, "land_name": "pusta", "user": False,
                                                 "passage": False}

        return map_array


class MoveController():
    def __init__(self):
        pass

    def get_partial_description(self, location, player):
        from game_app.command.execution.multiplayer import get_other_players_description
        from game_app.utils.ModuleManager import module_manager
        from interfaces.randomly_set_on_map import RandomlySetOnMap

        partial_description = LocationController.get_partial_description(location)
        other_players_description_response = get_other_players_description(player, location)
        #item_description = self.get_item_description(location, safe_mode)
        gold_description = self.get_gold_description(location)
        #asset_description = LocationController.get_asset_description(location)
        #monster_description = self.get_monster_description(location, player, safe_mode)

        message = partial_description

        if other_players_description_response.status == 'success':
            message += '<br><br>' + other_players_description_response.message

        if gold_description:
            message += '<br>' + gold_description

        for module in module_manager.get_all_modules().values():
            if issubclass(module.__class__, RandomlySetOnMap):
                message += '<br>' + module.get_all_desc_in_location(location)

        return message

    def get_full_description(self, location, player):
        message = self.get_partial_description(location, player)
        return LocationMessage(message).message

    def get_gold_description(self, location):
        message = ""
        gold = location.get_gold_in_map()
        if gold > 0:
            message = "<br>Na polu znajduje się " + GOLD.lower() + ": " + (Highlighter.bold(str(gold))) + "<br>"
        return message

    def execute_event_on_move(self, location, player):
        from game_app.utils.ModuleManager import module_manager
        from interfaces.randomly_set_on_map import RandomlySetOnMap

        for module in module_manager.get_all_modules().values():
            if issubclass(module.__class__, RandomlySetOnMap):
                module.set_on_map(location)
                module.fire_entry_event_in_location(location)

class LocationMessage:
    def __init__(self, message):
        self.message = message