# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.command.highlighter import Highlighter
from ..location import MoveController
from game_app.command.execution.others import Command
from ...models.MapModel import Map, MapLevel


class MoveCommand(Command):
    def __init__(self, request, direction):
        Command.__init__(self, request)
        self.direction = direction

    def move(self, player):
        from ...settings.command_settings import direction_coordinates

        target = None
        for offsetX, offsetY, directionName in direction_coordinates:
            if self.direction == directionName:
                target = player.location.is_direction_available(player.location.pk, offsetX, offsetY)

        if target is not None:
            player.location = target
            player.save()
            return player.location

        return []

    def execute(self):
        player = self.get_player_from_request()

        location = self.move(player)

        if location:

            moving_controller = MoveController()
            moving_controller.execute_event_on_move(location, player)
            message = moving_controller.get_full_description(location, player)

            return self.response.success(message)

        else:

            message = 'Wybrany kierunek prowadzi donikąd.'
            message = Highlighter.wrong_direction(message)
            return self.response.no_effect(message)


class TeleportCommand(Command):
    def execute(self):
        message_array = self.message_array
        player = self.get_player_from_request()

        if len(message_array) == 4:
            x = message_array[1]
            y = message_array[2]
            level = message_array[3]

            try:
                level = MapLevel.objects.get(level=level)
            except MapLevel.DoesNotExist:
                return self.response.no_effect("Taki poziom nie istnieje")

            try:
                map = Map.objects.get(x=x, y=y, level=level)
                player.set_location(map)
            except Map.DoesNotExist:
                return self.response.no_effect("Takie pole nie istnieje")

            command_object = LookCommand(self.request)
            return command_object.execute()

        else:
            message = "Zła składnia"
            return self.response.no_effect(message)


class LookCommand(Command):
    def execute(self):
        player = self.get_player_from_request()
        location = player.location

        moving_controller = MoveController()
        message = moving_controller.get_full_description(location, player)

        return self.response.success(message)


class PassageCommand(Command):
    def execute(self):

        message_array = self.message_array
        player = self.get_player_from_request()

        if len(message_array) > 1:

            del message_array[0]
            msg = " ".join(message_array)

            if player.enter_passage(msg) is False:
                message = "Brak przejścia o podanej nazwie"
                return self.response.no_effect(message)

            else:
                command_object = LookCommand(self.request)
                return command_object.execute()

        elif len(message_array) == 1 and player.get_passage():

            player.enter_passage(player.get_passage().name)
            command_object = LookCommand(self.request)
            return command_object.execute()

        else:
            message = "Składnia: %s nazwa_przejścia" % self.name
            return self.response.bad_syntax(message)


