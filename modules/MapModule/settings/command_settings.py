from .command_types import *
#from game_app.utils.ModuleManager import module_manager
from game_app.command.settings.command_definition import *
from ..command.execution.moving import MoveCommand,LookCommand,TeleportCommand

#discover_world_commands = list(filter(lambda x: x.type is MovingTypeCommand, module_manager.get_all_commands()))

#############################
# Discovering world
#
north = CommandProperties("północ", "pozwala poruszyć się na odpowiednie pole", MoveCommand, MovingTypeCommand, "w",
                          only_one_argument=True, order=1)
south = CommandProperties("południe", "analogicznie jak wyżej", MoveCommand, MovingTypeCommand, "s",
                          only_one_argument=True, order=2)
east = CommandProperties("wschód", "analogicznie jak wyżej", MoveCommand, MovingTypeCommand, "d",
                         only_one_argument=True, order=3)
west = CommandProperties("zachód", "analogicznie jak wyżej", MoveCommand, MovingTypeCommand, "a",
                         only_one_argument=True, order=4)
look = CommandProperties("spójrz", "zwraca położenie użytkownika z opisem pola", LookCommand, MovingTypeCommand, "l",
                         only_one_argument=True, order=5)
teleport = CommandProperties("teleport", "", TeleportCommand, CheatingTypeCommand)


##################################################
# coordinates settings and directions translation
#

direction_coordinates = [(0, 1, "south"), (1, 0, "east"), (0, -1, "north"), (-1, 0, "west")]

translate_polish_direction_to_english = {
    north.name: "north",
    south.name: "south",
    east.name: "east",
    west.name: "west"
}

translate_english_direction_to_polish = {v: k for k, v in translate_polish_direction_to_english.items()}