from game_app.utils.ModuleManager import module_manager
from .module_init import MapModuleInit
from .settings.command_settings import *


''' register module '''
if "MapModule" not in module_manager.get_all_modules():
    module_manager.register_module("MapModule", MapModuleInit)


''' register commands '''
if north not in module_manager.get_all_commands():
    module_manager.register_command(north)

if south not in module_manager.get_all_commands():
    module_manager.register_command(south)

if west not in module_manager.get_all_commands():
    module_manager.register_command(west)

if east not in module_manager.get_all_commands():
    module_manager.register_command(east)

if look not in module_manager.get_all_commands():
    module_manager.register_command(look)

if teleport not in module_manager.get_all_commands():
    module_manager.register_command(teleport)
